#! /usr/bin/python3.7

import click
from siteProcessor import scan, app
from siteProcessor import process_manual, buid_keys

@click.group()
def root():
    buid_keys()
    pass

@root.command()
def scan4jobs():
    """ starts the process of job shearching """

    click.secho('Starting Scan')
    jobsFromApi = scan('web', 'carlisle')
    click.secho('Scan Finished')

@root.command()
@click.option('--manual', default=False, help='Manual Processing of jobs.')
def apply4jobs(manual):
    """ automates the process of applying for jobs """
    if manual: 
        process_manual()
    else:
        click.secho('Starting application Process')
        app()
        click.secho('finished process')



if __name__ == '__main__':
    root()
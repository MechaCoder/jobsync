#! /usr/bin/python3.7

from click import *

class JobModual:

    def __init__(self, jobs: list):
        self.jobs = jobs

    def main(self):
        """ main function """

        for each in self.jobs:

            print(each)
            
            clear()
            secho('JobTitle')
            secho(f'> {each["jobTitle"]}')
            secho('Description')
            secho(f'> {each["jobDescription"]}')

            
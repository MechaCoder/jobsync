#! /usr/bin/python3.7
from os import path
from click import secho, prompt

from siteProcessor.reed.httpWorker import Http as reedHttp, HttpException
from siteProcessor.reed.webdriver import ReedWebdriver
from siteProcessor.uCredit.webdriver import Ucredit
from siteProcessor.totaljobs.webdriver import TotalJobs_Scanner, TotalJobs_apply4job
from siteProcessor.keys import SystemSettings
from siteProcessor.dataLayer import Jobs_database


class scanException(Exception): pass

def buid_keys():
    goodkeys = ['reed', 'reedPassword', 'reedEmail', 'uCeditUname', 'uCeditPword', 'tJobsUname', 'tJobsPassword']
    data = {}

    if path.isfile('./keys.json'):
        return False

    for key in goodkeys:
        data[key] = prompt('{} - >: '.format(key))
    SystemSettings().write(data)


def scan(keyWords:str, location:str):
    """ runs a scan of all jobsources sites and pulls them in to the database """
    sys = SystemSettings() # gets the system setings keys.json
    jobsDb = Jobs_database()
    reedDriver = ReedWebdriver()
    totalJobs = TotalJobs_Scanner(keyWords)

    try:
        reedKey = sys.get('reed')
        jobs = reedHttp(reedKey).getJobsList(keyWords, location)
        
        tjobs = totalJobs.process()
        for row in tjobs:
            
            if isinstance(row, dict) == False:
                print('not a dict')
                continue

            jobs.append(row)

        for row in jobs:
                row['applied'] = False
        
        if len(jobs) == 0:
            raise scanException('there a no jobs to apply for')

        jobsDb.create_multiple(jobs)

        return jobs
    except HttpException as err:
        raise scanException('there has been issue with the http call')

    # except Exception as err:
    #     print(err)

def app():
    sys = SystemSettings()
    jobs = Jobs_database()
    
    try:
        reed_jobs = jobs.readAllByAppliedandSourse()['reed']
    except KeyError as err:
        reed_jobs = []
    
    try:
        total_jobs = jobs.readAllByAppliedandSourse()['totaljobs']
    except KeyError as err:
        total_jobs = []
    reed_jobs_url = []

    for row in reed_jobs:
        reed_jobs_url.append(row['job_url'])
        
    appliedForJobs = ReedWebdriver().applyForJob(reed_jobs_url, sys.get('reedEmail'), sys.get('reedPassword')) ## applying for jobs
    for jobUrl in appliedForJobs:
        jobs.updateAppliedByUrl(jobUrl)

    appliedForJobs = TotalJobs_apply4job().apply4jobs(total_jobs, sys.get('tJobsUname'), sys.get('tJobsPassword'))
    for jobUrl in appliedForJobs:
        jobs.updateAppliedByUrl(jobUrl)

    return True

def sync():
    return False
    sys = SystemSettings()
    web = Ucredit()
    allJobsSysnc = Jobs_database().readBySync()
    web.syncJobs(allJobsSysnc, sys.get('uCeditUname'), sys.get('uCeditPword'))
    
def process_manual():
    sysObjs = SystemSettings()
    jobs = Jobs_database()

    reed_jobs = jobs.readAllByAppliedandSourse()
    # reed_jobs_url = []
    email = sysObjs.get('reedEmail')
    passwd = sysObjs.get('reedPassword')

    tEmail = sysObjs.get('tJobsUname')
    tPassWord = sysObjs.get('tJobsPassword')

    doneJobs = []
    if 'reed' in reed_jobs.keys() and len(reed_jobs['reed']) != 0:
        doneJobs.append(
            ReedWebdriver().applyManual(reed_jobs['reed'], email, passwd)
        )

    if 'totaljobs' in reed_jobs.keys() and len(reed_jobs['totaljobs']) != 0:
        doneJobs.append(
            TotalJobs_apply4job().manualApply(reed_jobs['totaljobs'], tEmail, tPassWord)
        )

    for url in doneJobs:
        jobs.updateAppliedByUrl(url)
    

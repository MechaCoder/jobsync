from json import loads, dumps

class SystemSettingsException(Exception): pass

class SystemSettings:

    def __init__(self, fileLoc: str = './keys.json'):
        self.file = fileLoc

    def _read(self):
        "returns elements a json decode dict of the setting file"

        try:
            fileObj = open(self.file)
            jsonstring = fileObj.read()
            fileObj.close()

            return loads(jsonstring)

        except FileNotFoundError as err:
            raise SystemSettingsException('keys.json has not been found')

        except TypeError as Err:
            raise SystemSettingsException('settings file is invaild and can not be used')

    def get(self, key: str):
        """ gets the value from the settings file """

        settings = self._read()
        if key not in settings.keys():
            raise SystemSettingsException('setting not found')

        return settings[key]

    def write(self, data: dict):

        jsonSting = dumps(data)
        fileObj = open(self.file, mode="w")
        writen = fileObj.write(jsonSting)
        fileObj.close()
        return True
from urllib.request import Request, urlopen
from time import sleep
from socket import timeout
from webbrowser import open_new

from bs4 import BeautifulSoup
from click import progressbar, secho
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotInteractableException, ElementClickInterceptedException

# scanner
# contents = urlopen('https://www.totaljobs.com/jobs/junior-front-end-developer').read()

class TotalJobs_Scanner_Exception(Warning): pass

class TotalJobs_Scanner:

    def __init__(self, jobtitle: str):
        """ scanns total jobs """
        self.jobtitle =  jobtitle.replace(' ', '-')

    def __makePageUrlJobTitle(self, pageNum:int=1):
        return f'https://www.totaljobs.com/jobs/{self.jobtitle}?page={pageNum}'

    def __genrateHeraders(self):
        return {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}

    def processPage(self, pageNum:int):
        # rawHtml = Request( self.__makePageUrlJobTitle(pageNum=pageNum), headers=self.__genrateHeraders() )
        html = urlopen(self.__makePageUrlJobTitle(pageNum=pageNum), timeout=5)

        if html.status != 200:
            raise TotalJobs_Scanner_Exception('the html status is not 200')
        
        soup = BeautifulSoup(html.read(), 'html.parser')
        els = soup.select('.job-results .job')

        rows = []

        with progressbar(els) as bar:
            for each in bar:

                jobid = each.get('id')
                jobSoup = BeautifulSoup(str(each), 'html.parser')
                title = jobSoup.select('.job-title h2')[0].string
                descp = jobSoup.select('.job-intro')[0].text
                url = jobSoup.select('.job-title a')[0].get('href')

                rows.append({
                    'job_id': jobid,
                    'job_title': title,
                    'job_descp': descp,
                    'job_url': url,
                    'employer_name': '',
                    'source': 'totaljobs'
                })

        return rows

    def process(self):
        
        pagenum = 1
        rows = []
        with progressbar(range(0, 5), label='processing pages from total jobs') as bar:
            for e in bar:
                try:
                    rows.extend(self.processPage(pagenum))
                    sleep(10)
                    pagenum = pagenum + 1
                    continue
                except TotalJobs_Scanner_Exception as err:
                    print(err)
                    break

                except timeout as err:
                    print('a page timed out')
                    break
        return rows

class TotalJobs_apply4job:

    def __login__(self, uName: str, pWord: str):

        converted = lambda inputStr: u'{}'.format(inputStr)

        driver = Firefox()
        driver.get('https://www.totaljobs.com/account/signin?ReturnUrl=/')
        
        uNameEl = driver.find_element_by_id('Form_Email')
        uNameEl.clear()
        uNameEl.send_keys( converted(uName) )

        pWordEl = driver.find_element_by_id('Form_Password')
        pWordEl.clear()
        pWordEl.send_keys( converted(pWord) )

        btn = driver.find_element_by_id('btnLogin')
        btn.click()

        return driver

    def apply4jobs(self, rows: list, uName: str, pWord: str):

        doneUrls = []

        driver = self.__login__(uName, pWord)

        with progressbar(rows, label='applying for jobs on totaljobs') as bar:
            for row in bar:

                try:

                    driver.get(row['job_url'])
                    applybtn = driver.find_element_by_id('JobToolsTop_AOLOptions_lnkApplyOnline')
                    applybtn.click()

                    applybtn2 = driver.find_element_by_id('btnSubmit')
                    applybtn2.click()

                    doneUrls.append(row['job_url'])

                except NoSuchElementException as err:
                    doneUrls.append(row['job_url'])
                except ElementClickInterceptedException as err:
                    doneUrls.append(row['job_url'])
                except ElementNotInteractableException as err:
                    # the ElementNotInteractableException is becouse job has already been applyed for.
                    # ElementClickInterceptedException this is becouse there are elements that need 
                    # to be done manuals
                    pass

        driver.quit()
        return doneUrls

    def manualApply(self, rows: list, uName: str, pWord: str):

        sucssusfull = []
        jobInt = 0
        for job in rows:
            jobInt += 1
            secho('Job {} of {}'.format(jobInt, len(rows)))
            open_new(job['job_url'])
            sucssusfull.append(job['job_url'])
            break
        
        return sucssusfull[0]



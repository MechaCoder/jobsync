from click import progressbar

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException

class Ucredit:

    def syncJobs(self, rows: list, uName: str, pWord: str, pWord2: str):
        """ sysncs all jobs with uCredit """

        #login to uCredit
        converted = lambda inputStr: u'{}'.format(inputStr)

        driver = Firefox()
        driver.get('https://www.universal-credit.service.gov.uk/sign-in')
        unameEl = driver.find_element_by_id('id-userName')
        unameEl.clear()
        unameEl.send_keys( converted(uName) )
        pWordEl = driver.find_element_by_id('id-password')
        pWordEl.clear()
        pWordEl.send_keys( converted(pWord) )
        btn = driver.find_element_by_id('id-submit-button')
        btn.click()

        pWord2El = driver.find_element_by_id('id-answer')
        pWord2El.clear()
        pWord2El.send_keys( converted(pWord2) )
        btn = driver.find_element_by_id('id-submit-button')
        btn.click()
        
        # this takes to the point where i am logged in to uCredit
        driver.get('https://www.universal-credit.service.gov.uk/work-search')
        

        # adding the rows to the jobSync.
        with progressbar(rows, label="syncing Jobs to unversal credit site") as bar:
            for row in bar:

                if isinstance(row, dict) is False:
                    continue

                if 'job_title' not in row.keys():
                    continue

                if 'job_description' not in row.keys():
                    continue

                if 'employer_name' not in row.keys():
                    continue

                addJobbtn = driver.find_element_by_id('add-job')
                addJobbtn.click()

                jobTitle = driver.find_element_by_id('id-jobTitle')
                jobTitle.clear()
                jobTitle.send_keys(converted(row['job_title']))

                employer = driver.find_element_by_id('id-employer')
                employer.clear()
                employer.send_keys(converted(row['employer_name']))

                statusEl = driver.find_element_by_id('clickable-APPLIED')
                statusEl.click()

                notesEl = driver.find_element_by_id('id-notes')
                notesEl.clear()
                notesEl.send_keys(converted(row['job_description']))

                submitBtn = driver.find_element_by_id('id-submit-button')
                submitBtn.click()
            # submited job
        return True
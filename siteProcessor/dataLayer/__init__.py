# !/usr/bin/python3.7

from tinydb import TinyDB
from tinydb import Query
from tinydb.database import Document

class Database:

    def __init__(self, filePath: str = './ds.json'):
        self.filepath = filePath
        self.table = '_defualt'

    def create_multiple(self, rows: list):

        database = TinyDB(self.filepath)
        table = database.table(self.table)

        docids = table.insert_multiple(rows)

        database.close()
        return docids

    def __output__(self, row: Document):

        temp = {
            'doc_id': row.doc_id,
        }

        for key in row.keys():
            temp[key] = row[key]
        return temp

class Jobs_database(Database):

    def __init__(self, filePath: str = './ds.json'):
        super().__init__(filePath)
        self.table = 'jobs'

    def create(self, ident: int, title: str, descp: str, url: str, employer: str, source: str, applied: bool=False, sync=False):

        database = TinyDB(self.filepath)
        table = database.table(self.table)

        docid = table.insert({
            'job_id': ident,
            'job_title': title,
            'job_description': descp,
            'job_url': url,
            'employer_name': employer,
            'applied': applied,
            'sync': sync,
            'source': source
        })

        database.close()
        return docid

    def create_multiple(self, rows: list):

        database = TinyDB(self.filepath)
        table = database.table(self.table)

        goodRows = []
        for row in rows:
            t = row
            if 'applied' not in row.keys():
                t['applied'] = False

            if table.contains(Query().job_id == row['job_id']):
                continue

            goodRows.append(t)  

        docids = table.insert_multiple(rows)

        database.close()
        return docids

    def readAllByApplied(self):
        """ returns all jobs that a have applied=False """
        
        database = TinyDB(self.filepath)
        table = database.table(self.table)

        returnRows = []
        for row in table.search(Query().applied == False):
            returnRows.append(
                self.__output__(row)
            )
        database.close()
        return returnRows

    def readByJobUrl(self, url: str):

        database = TinyDB(self.filepath)
        table = database.table(self.table)

        returnList = []
        rows = table.search(Query().job_url == url)
        for row in rows:
            returnList.append(self.__output__(row))
        return returnList

    def updateAppliedByUrl(self, url: str):
        
        database = TinyDB(self.filepath)
        table = database.table(self.table)

        updated = table.update({'applied': True}, Query().job_url == url)

        database.close()
        return updated

    def readBySync(self):

        database = TinyDB(self.filepath)
        table = database.table(self.table)
        
        returnRows = []
        for row in table.search(Query().sync == False):
            returnRows.append(
                self.__output__(row)
            )
        database.close()
        return returnRows

    def updateMassSync(self):
        database = TinyDB(self.filepath)
        table = database.table(self.table)
        table.update({'sync': True})
        database.close()

    def readAllByAppliedandSourse(self):
        database = TinyDB(self.filepath)
        table = database.table(self.table)

        rObject = {}
        for row in table.search(Query().applied == False):
            if row['source'] not in rObject.keys():
                rObject[row['source']] = []
            
            rObject[row['source']].append( self.__output__(row) )

        return rObject

        
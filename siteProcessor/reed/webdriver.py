from webbrowser import open_new

from click import progressbar
from click import secho, prompt, clear

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotInteractableException, ElementClickInterceptedException


class ReedWebdriver:

    def __login__(self, email: str, passwd: str):

        converted = lambda inputStr: u'{}'.format(inputStr)
        driver = Firefox()
        
        driver.get('https://www.reed.co.uk/account/signin#&card=signin')
        uname = driver.find_element_by_id('Credentials_Email')
        uname.clear()
        uname.send_keys( converted(email) )

        passWord = driver.find_element_by_id('Credentials_Password')
        passWord.clear()
        passWord.send_keys( converted(passwd) )
        btn = driver.find_element_by_id('signin-button')
        btn.click()

        return driver

    def applyForJob(self, jobList:list, email, passwd):

        driver = self.__login__(email, passwd)

        sucssusfull = []

        with progressbar(jobList, label="Apllying for jobs on Reed") as bar:
            for job in bar:
                try:
                    driver.get(job)
                    applybtn = driver.find_element_by_id("applyButtonSide")
                    applybtn.click()

                    applybtn2 = driver.find_element_by_id("sendApplicationButtonBottom")
                    applybtn2.click()

                    sucssusfull.append(job)
                except NoSuchElementException as err:
                    sucssusfull.append(job)
                except ElementNotInteractableException or ElementClickInterceptedException as err:
                    # the ElementNotInteractableException is becouse job has already been applyed for.
                    # ElementClickInterceptedException this is becouse there are elements that need 
                    # to be done manuals
                    pass
                
        driver.close()
        return sucssusfull
    
    def applyManual(self, jobList: list, email: str, passwd: str):
        
        sucssusfull = []
        jobInt = 0
        for job in jobList:
            
            jobInt += 1
            secho('Job {} of {}'.format(jobInt, len(jobList)))
            open_new(job['job_url'])
            sucssusfull.append(job['job_url'])
            break

        return sucssusfull[0]

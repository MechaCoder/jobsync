#! /usr/bin/python3.7

from requests import get

class HttpException(Exception): pass

class Http:

    def __init__(self, apiKey:str):
        self.key = apiKey

    def getJobsList(self, keyword:str, location:str):
        """ returns a list of jobs in by keyword and loation """

        shearchStr = f'https://www.reed.co.uk/api/1.0/search?keywords={keyword}&location={location}'

        r = get(
            shearchStr,
            auth=(self.key, '')
        )

        if r.status_code == 401:
            raise HttpException(f'your apikey is not valid')

        if r.status_code != 200:
            raise HttpException(f'there is an issue request obect {r.status_code}')

        payload = r.json()

        rList = []

        for job in payload['results']:
            
            keys = job.keys()
            item = {}
            
            item['job_id'] = job['jobId']
            item['job_title'] = job['jobTitle']
            item['job_description'] = job['jobDescription']
            item['job_url'] = job['jobUrl']
            item['employer_name'] = job['employerName']
            item['source'] = 'reed'
            
            rList.append(item)

        return rList

    def getJobDetail(self, jobId:int):

        shearchStr = f'https://www.reed.co.uk/api/1.0/jobs/{jobId}'

        r = get(
            shearchStr,
            auth=(self.key, '')
        )

        if r.status_code == 401:
            raise HttpException(f'your apikey is not valid')

        if r.status_code != 200:
            raise HttpException(f'there is an issue request obect {r.status_code}')

        payload = r.json()
        return payload
        
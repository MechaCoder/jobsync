# JobSync a tool for jobSeekers

```JobSync``` is a tool written in ```python 3.7``` to assist job seekers to automate, the job search this is an achieved by enabling the user to apply for as many jobs as possible on the supported platforms, this ensures that the user can achieve the maximum possible market penetration.

the software is interacted by currently python3.7 and pipenv and comes with a few commands because this is currently not packaged. this means that you can use the commands by using `pipenv run app.py <command> <options>`;

| command | what does it do |
|---|---|
|`scan4jobs`|this command will scan all supported platforms for jobs using the read API and first the first six pages of total jobs|
|`apply4jobs`| this will automatically apply for all the jobs on totaljobs and reed.|

## To Do;
+ adding a method to dynamically script the keywords and location for the job search
+ adding a command to sync with UC
